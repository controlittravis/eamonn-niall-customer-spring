package com.citi.training.customer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.customer.exceptions.CustomerNotFoundException;
import com.citi.training.customer.model.Customer;

@Component
public class InMemoryCustomerDao implements ICustomerDao  {

		private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

		public void saveCustomer(Customer customer) {
			allCustomers.put(customer.getId(), customer);
		}

		public Customer getCustomer(int id) {
			if(allCustomers.get(id)==null) {
				throw new CustomerNotFoundException("There is no Customer found");
			}else {
			return allCustomers.get(id);
			}
		}

		public List<Customer> getAllCustomers() {
			return new ArrayList<Customer>(allCustomers.values());
		}

		public void removeCustomer(int id) {
			Customer removedCustomer =  allCustomers.remove(id);

			if(removedCustomer==null) {
				throw new CustomerNotFoundException("Customer Not Found: Can not be removed");
			}
			
		}

	}


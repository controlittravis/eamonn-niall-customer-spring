package com.citi.training.customer.rest;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.customer.dao.ICustomerDao;
import com.citi.training.customer.model.Customer;

@RestController
@RequestMapping("/customer") // end point of URL and methods to be run
public class CustomerController {

	@Autowired
	ICustomerDao customerRepository;
	
	private static Logger LOG = org.slf4j.LoggerFactory.getLogger(CustomerController.class);

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> addCustomer(@RequestBody Customer customer) {
		LOG.info("Saving new customer. ID:" + customer.getId());
		customerRepository.saveCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Customer> getAll() {
		LOG.info("Getting all customers");
		return customerRepository.getAllCustomers();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, 
										produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer get(@PathVariable int id) {
		LOG.info("Getting customer: " + id);
		return customerRepository.getCustomer(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value= HttpStatus.NO_CONTENT)
	public void remove(@PathVariable int id) {
		LOG.info("Removing customer: " + id);
		customerRepository.removeCustomer(id);
	}

}


package com.citi.training.customer.demo;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.customer.dao.ICustomerDao;
import com.citi.training.customer.model.Customer;

@Component
public class Demo implements ApplicationRunner {

    @Autowired
    private ICustomerDao customerRepository;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some Customers");
        String[] customerNames = {"John", "Mary", "Conor"};
        String[] customerAddresses = {"Stranmillis", "Ormeau", "Holylands"};
        

        for(int i=0; i<customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                                              customerNames[i],
                                              customerAddresses[i]);

            System.out.println("Created Customer: " + thisCustomer);
            customerRepository.saveCustomer(thisCustomer);
        }

        System.out.println("\nAll Customers:");
        for(Customer customer: customerRepository.getAllCustomers()) {
            System.out.println(customer);
        }
    }

  
}

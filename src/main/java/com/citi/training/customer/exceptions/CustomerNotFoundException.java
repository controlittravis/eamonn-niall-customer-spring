package com.citi.training.customer.exceptions;

@SuppressWarnings("serial")
public class CustomerNotFoundException extends RuntimeException {
	
	public CustomerNotFoundException(String errorMsg) {
		super(errorMsg);
	}

}

package com.citi.training.customer.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.customer.dao.InMemoryCustomerDao;
import com.citi.training.customer.exceptions.CustomerNotFoundException;
import com.citi.training.customer.model.Customer;

@RestController
public class InMemCustomerTest {

	private static Logger LOG = LoggerFactory.getLogger(InMemCustomerTest.class);

    private int testId = 5;
    private String testName = "Eamonn";
    private String testAddress = "70 Zoo Road";

    @Test
    public void test_saveAndGetCustomer() {
        Customer testCustomer = new Customer(testId, testName, testAddress);
        InMemoryCustomerDao testRepo = new InMemoryCustomerDao();

        testRepo.saveCustomer(testCustomer);
 
        assertTrue(testRepo.getCustomer(testId).equals(testCustomer));

    }

    @Test
    public void test_saveAndGetAllCustomers() {
        Customer[] testCustomerArray = new Customer[100];
        InMemoryCustomerDao testRepo = new InMemoryCustomerDao();

        for(int i=0; i<testCustomerArray.length; ++i) {
            testCustomerArray[i] = new Customer(testId + i, testName, testAddress + i);

            testRepo.saveCustomer(testCustomerArray[i]);
        }

        List<Customer> returnedCustomers = testRepo.getAllCustomers();
        LOG.info("Received [" + returnedCustomers.size() +
                 "] Customers from Dao");

        for(Customer thisCustomer: testCustomerArray) {
            assertTrue(returnedCustomers.contains(thisCustomer));
        }
        LOG.info("Matched [" + testCustomerArray.length + "] Customers");
    }
    @Test (expected = CustomerNotFoundException.class)
    public void testDeleteCustomerNotFoundException() {
    	InMemoryCustomerDao testRepo = new InMemoryCustomerDao();
        testRepo.removeCustomer(500);
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void testGetCustomerNotFoundException() {
    	InMemoryCustomerDao testRepo = new InMemoryCustomerDao();
        testRepo.getCustomer(500);
    	
    }
}
